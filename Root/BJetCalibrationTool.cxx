///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/

// BJetCalibrationTool.cxx
// Source file for class BJetCalibrationTool
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
/////////////////////////////////////////////////////////////////// 


// BJetCalibrationTool includes
#include <AsgTools/MessageCheckAsgTools.h>
#include "BJetCalibrationTool/BJetCalibrationTool.h"
#include "PathResolver/PathResolver.h"

// Constructors
////////////////

BJetCalibrationTool::BJetCalibrationTool(const std::string& name) : asg::AsgTool (name)
{ 
}

StatusCode BJetCalibrationTool::initialize() {
	
  ANA_MSG_INFO ("Initializing BJetCalibrationTool ... " << name() << " ... "); 
  ANA_CHECK( this->initializeTool( name() ) );
  return StatusCode::SUCCESS;
}

StatusCode BJetCalibrationTool::initializeTool(const std::string& name) {

  if(m_JetAlgo == "")
  {
	ANA_MSG_FATAL("BJetCalibrationTool::initialize() : Please set JetCollection"); 
	return StatusCode::FAILURE;
  }

  if(m_PtReco == "")
  {
	ANA_MSG_FATAL("BJetCalibrationTool::initialize() : Please set PtReco file name");
	return StatusCode::FAILURE;
  }
  ANA_CHECK(m_muonSelection.retrieve());
  
  if(m_doPtReco)
  {
  	TString filename = PathResolverFindCalibFile("BJetCalibrationTool/"+m_JetAlgo+"_"+m_PtReco);
	m_PtRecoFile = std::make_unique<TFile>(filename,"READ");
	if(!m_PtRecoFile)
        {
		ANA_MSG_FATAL("BJetCalibrationTool::initialize() : PtReco file is not found" << filename );
		return StatusCode::FAILURE;
        }
	m_Semi_Histo = (TH1F*) m_PtRecoFile->Get(("Correction_SemiLeptonic_"+m_BJet_Tagger+"_ttbar_mean").c_str());
	m_Had_Histo = (TH1F*) m_PtRecoFile->Get(("Correction_Hadronic_"+m_BJet_Tagger+"_ttbar_mean").c_str());
	
	if(m_Semi_Histo == NULL or m_Had_Histo == NULL)
	{
		ANA_MSG_FATAL("BJetCalibrationTool::initialize() : Please check the histograms names");
		return StatusCode::FAILURE;
	}

  }
  ANA_MSG_INFO("BJetCalibrationTool::initialize() : " << name << " is initialized!");
  return StatusCode::SUCCESS;
}

std::vector< const xAOD::Muon* > BJetCalibrationTool::selectMuonsForCorrection(const xAOD::MuonContainer& muons)
{
	std::vector< const xAOD::Muon* > selMuons;
	selMuons.reserve(muons.size());
	// Should really use m_muonSelection->accept(*mu)??
	auto acceptMuon = [this](const xAOD::Muon* mu) {
		return this->m_muonSelection->accept(*mu);
	};
	std::copy_if(
		muons.begin(), muons.end(), std::back_inserter(selMuons),
		acceptMuon);
	return selMuons;
}

StatusCode BJetCalibrationTool::applyBJetCalibration(xAOD::Jet& jet,const std::vector< const xAOD::Muon* >& muons) { 

  std::vector< const xAOD::Muon* > muons_in_jet = getMuonInJet(jet, muons);

  addMuon(jet, muons_in_jet);
 
  if(m_doPtReco)
  {
	applyPtReco(jet);
  }

  muons_in_jet.clear();
  return StatusCode::SUCCESS; 
}

void BJetCalibrationTool::applyPtReco(xAOD::Jet& jet)
{
	const static SG::AuxElement::Decorator<float> dec_PtRecoF("PtReco_SF");
	const static SG::AuxElement::Decorator<float> dec_MuonCorrPt("muonCorrPt");

	int nmu = jet.auxdata<int>("n_muons");
	float PtRecoFactor = 1.;
	TLorentzVector j = jet.p4();

	dec_MuonCorrPt(jet) = j.Pt();

	if(nmu == 0)
	{
		PtRecoFactor = m_Had_Histo->Interpolate(log(j.Pt() * 0.001));

	}else if(nmu > 0){
	
		PtRecoFactor = m_Semi_Histo->Interpolate(log(j.Pt() * 0.001));
	}	
		
	j *= PtRecoFactor;
	
	xAOD::JetFourMom_t new_jet(j.Pt(),j.Eta(),j.Phi(),j.M());
	jet.setJetP4(new_jet);
	dec_PtRecoF(jet) = PtRecoFactor;

}

std::vector< const xAOD::Muon* > BJetCalibrationTool::getMuonInJet(xAOD::Jet& jet, const std::vector< const xAOD::Muon* >& muons)
{

	std::vector< const xAOD::Muon* > m_muon_in_jet;

	for( unsigned int i = 0; i<muons.size(); i++)
	{
		TLorentzVector mu = muons[i]->p4();	
		TLorentzVector j = jet.p4();	
		Double_t dR = j.DeltaR(mu);
			
		if(m_doVR)
		{
		   m_Jet_Muon_dR = std::min(0.4, 0.04 + (10/ (((mu).Pt() * 0.001))));	
		} 		
		
		if(dR > m_Jet_Muon_dR ) continue;

		m_muon_in_jet.push_back(muons[i]);
		
	}
	return m_muon_in_jet;
}

void BJetCalibrationTool::addMuon(xAOD::Jet& jet, const std::vector< const xAOD::Muon* >& muons)
{
	const static SG::AuxElement::Decorator<int> dec_NMu("n_muons");
	const static SG::AuxElement::ConstAccessor<int> cacc_NMu("n_muons");
	
	dec_NMu(jet) = muons.size();
	
	if(cacc_NMu(jet) > 0)
	{
		const xAOD::Muon* muon = getMuon(muons);
	
		TLorentzVector mu = muon->p4();
		TLorentzVector j = jet.p4();
		float eLoss = 0.0;
		muon->parameter(eLoss,xAOD::Muon::EnergyLoss);
		double theta=mu.Theta();
  		double phi=mu.Phi();
  		double eLossX=eLoss*sin(theta)*cos(phi);
  		double eLossY=eLoss*sin(theta)*sin(phi);
  		double eLossZ=eLoss*cos(theta);
		TLorentzVector Loss = TLorentzVector(eLossX,eLossY,eLossZ,eLoss);

		j= j-Loss+mu;		
		
		xAOD::JetFourMom_t new_jet(j.Pt(),j.Eta(),j.Phi(),j.M());

		jet.setJetP4(new_jet);
	}
}
const xAOD::Muon* BJetCalibrationTool::getMuon(const std::vector< const xAOD::Muon* >& muons)
{
	int muon_size = muons.size();
	Double_t max_muon_pt = muons[0]->pt();
	int index = 0;	
	for(int i = 0; i<muon_size; i++)
	{
		if(max_muon_pt < muons[i]->pt())
		{
			max_muon_pt = muons[i]->pt();
			index = i;
		}
	}
	return muons[index];
 
}
